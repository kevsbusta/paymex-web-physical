import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable'
import { Login } from './login';
import { LoginPostModel } from './login-post-model';

@Injectable()
export class LoginService {
  private url: string = 'http://localhost:56735/api/login';
  private headers = new HttpHeaders({'Content-Type': 'application/json'});

  constructor(private http: HttpClient) { }
  login (login: Login) : Observable<Object> { 
    var postModel = this.toPostModel(login);
    let bodyString = JSON.stringify(postModel); 
    return this.http.post(this.url, bodyString, {headers: this.headers});
  }

  private toPostModel(login: Login) : LoginPostModel {
    var result = new LoginPostModel();
    result._password = login.password;
    result._username = login.username;

    return result;
  }
}
