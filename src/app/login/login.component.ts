import { Component, OnInit } from '@angular/core';
import { LoginService } from './login.service';
import { Login } from './login';
import { HttpResponse } from '@angular/common/http';
import { resetFakeAsyncZone } from '@angular/core/testing';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [LoginService]
})
export class LoginComponent implements OnInit {

  private loginModel: Login = new Login();
  constructor(private loginService: LoginService, private router: Router) { }

  ngOnInit() {
  }

  Login(): void {
    this.loginService.login(this.loginModel).subscribe((token: string) => {
      if(token != null && token != "") {
        console.log(token);
        localStorage.setItem('userToken', token);
        this.router.navigate(['/transaction']);
      }
    });
  }
}
