export class TransactionPostModel {
    name: string;
    description: string;
    amount: number;
    key: string;
    expiry_date: Date;
}
