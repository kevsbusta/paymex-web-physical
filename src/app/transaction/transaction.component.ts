import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router } from "@angular/router";
import { Transaction } from './transaction';
import { TransactionService } from './transaction.service';
import { PatternValidator } from '@angular/forms';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-transaction',
  templateUrl: './transaction.component.html',
  styleUrls: ['./transaction.component.css'],
  providers: [TransactionService]
})
export class TransactionComponent implements OnInit {

  private transaction: Transaction;
  private mask: any = [/\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];

  constructor(
      private router: Router, 
      private transactionService: TransactionService,
      private toastr: ToastsManager,
      vcr: ViewContainerRef) {
        this.toastr.setRootViewContainerRef(vcr);
        this.transaction = new Transaction();  
  }

  ngOnInit() {
  }

  Logout(): void {
    localStorage.removeItem('userToken');
    this.router.navigate(['/login'])
  }

  checkIfValid(): boolean {
    return this.transaction.name != "" 
            && this.transaction.amount != null
            && this.transaction.expiryDate != null
            && this.transaction.paymexKey != ''
            && this.transaction.description != '';
  }

  Submit(): void {
    if(this.checkIfValid()) {
      this.transactionService.submit(this.transaction).subscribe((response) => {
        console.log(response);
        this.toastr.success('Valid Paymex key! Transaction done.', 'Success!');
      }, (err: HttpErrorResponse) => {
        var error = JSON.parse(err.error);
        this.toastr.error(error.message, 'Error!');
      });
    }
  }
}
