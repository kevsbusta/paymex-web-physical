import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable'
import { Transaction } from './transaction';
import { TransactionPostModel } from './transaction-post-model';
import { resetFakeAsyncZone } from '@angular/core/testing';

@Injectable()
export class TransactionService {
  private url: string = 'http://58e9a765.ngrok.io/physical_merchant_payments';
  private headers = new HttpHeaders({'Content-Type': 'application/json'});

  constructor(private http: HttpClient) { }
  submit (transaction: Transaction) : Observable<any> { 
    var postModel = this.toRubyPostString(transaction);
    let bodyString = JSON.stringify(postModel); 
    return this.http.post(this.url, bodyString, {headers: this.headers, responseType: 'text'});
  }

  private toRubyPostString(transaction: Transaction) : TransactionPostModel {
    var result = new TransactionPostModel();
    result.amount = transaction.amount;
    result.description = transaction.description || "";
    result.expiry_date = transaction.expiryDate;
    result.name = transaction.name;
    result.key = transaction.paymexKey.replace(/-/g, "");

    return result;
  }
}
