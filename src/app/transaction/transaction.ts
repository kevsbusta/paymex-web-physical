export class Transaction {
    name: string;
    description: string;
    amount: number;
    paymexKey: string;
    expiryDate: Date;
}
